const API_KEY = "cbd8f41f59d794206f93354b462fd69f9f7bd4bee40f9e98bf0f710e3f23d8f5";
const API_KEY_PARAM = "api_key";

const socketURL = new URL('wss://streamer.cryptocompare.com/v2');
const searchParams = socketURL.searchParams;
searchParams.set(API_KEY_PARAM, API_KEY);

const socket = new WebSocket(socketURL.toString());

const ports = [];

socket.addEventListener("message", e => {
    ports.forEach(p => {
        p.postMessage({
            swMessage: e.data
        })
    })
})

function sendToWebSocket(message) {
    if (socket.readyState === WebSocket.OPEN) {
        socket.send(message);
        return;
    }

    socket.addEventListener("open", () => {
        socket.send(message);
    }, {once: true})
}

function handlePortMessage(data) {
    sendToWebSocket(JSON.stringify(data))
}

self.onconnect = e => {
    const port = e.ports[0];
    port.onmessage = e => {
        handlePortMessage(e.data);
    }
    ports.push(port);
}