/*const API_REQUEST_URL = "https://min-api.cryptocompare.com/data/pricemulti";
const API_FROM_VALUES_PARAM = "fsyms";
const API_TO_VALUES_PARAM = "tsyms";
const API_UPADATE_INTERVAL_MS = 5000;*/
import axios from "axios";

const API_CURRENCY_LIST_URL = "https://min-api.cryptocompare.com/data/all/coinlist?summary=true";

const MAIN_CURRENCY = "USD";
const SECOND_CURRENCY = "BTC";
const AGREGATE_INDEX = "5";
const SUB_FAIL_INDEX = "500";
const API_MESSAGE_INVALID_SUB = "INVALID_SUB";

const tickersSubscriptions = new Map();

let defaultExchangePrice = null;

const sWorker = new SharedWorker('@/workers/sWorker.js', {type: 'module'});

subscribeOnDefaultExchangePair();

sWorker.port.onmessage = e => {
    const {TYPE: type, FROMSYMBOL: currency, PRICE: newPrice, MESSAGE: message, PARAMETER: parameter} = JSON.parse(e.data.swMessage);

    if (type === SUB_FAIL_INDEX && message === API_MESSAGE_INVALID_SUB) {
        const failedCurrencyPair = getFailedCurrencyFromMessage(parameter);
        if (!failedCurrencyPair) {
            return;
        }

        if (failedCurrencyPair.to === SECOND_CURRENCY) {
            const errorHandlers = tickersSubscriptions.get(failedCurrencyPair.from).errorCallbacks ?? [];
            errorHandlers.forEach(handler => handler(message));
            return;
        }
        unsubscribeFromTickerOnWs(failedCurrencyPair.from);
        subscribeToTickerOnWs(failedCurrencyPair.from, SECOND_CURRENCY);
        tickersSubscriptions.get(failedCurrencyPair.from).subFailed = true;
        return;
    }

    if (type !== AGREGATE_INDEX) {
        return;
    }

    if (newPrice === undefined) {
        return;
    }

    if (currency === SECOND_CURRENCY) {
        defaultExchangePrice = newPrice;
    }

    const handlers = tickersSubscriptions.get(currency).callbacks ?? [];
    const isSubFailed = tickersSubscriptions.get(currency).subFailed;
    handlers.forEach(handler => handler(isSubFailed ? convertPriceForSecondaryExchange(newPrice) : newPrice));
}

function convertPriceForSecondaryExchange(price) {
    return price * defaultExchangePrice;
}

function getFailedCurrencyFromMessage(message) {
    const matchResult = new RegExp("5~CCCAGG~(.*)~(.*)").exec(message);
    if (matchResult) {
        return {
            from: matchResult[1],
            to: matchResult[2]
        }
    }
    return null;
}

function subscribeOnDefaultExchangePair () {
    sendToWebSocket({
        action: "SubAdd",
        subs: [`5~CCCAGG~${SECOND_CURRENCY}~${MAIN_CURRENCY}`]
    })
}

function subscribeToTickerOnWs(tickerName, toCurrency) {
    sendToWebSocket({
        action: "SubAdd",
        subs: [`5~CCCAGG~${tickerName}~${toCurrency ? toCurrency : MAIN_CURRENCY}`]
    })
}

function unsubscribeFromTickerOnWs(tickerName, toCurrency) {
    sendToWebSocket({
        action: "SubRemove",
        subs: [`5~CCCAGG~${tickerName}~${toCurrency ? toCurrency : MAIN_CURRENCY}`]
    })
}

function sendToWebSocket(message) {
    sWorker.port.postMessage(message);
}

export const subscribeToTickerUpdate = (tickerName, callback, onErrorCallback) => {
    const subscribers = tickersSubscriptions.get(tickerName) || {
        callbacks: [],
        errorCallbacks: [],
        subFailed: false
    };
    tickersSubscriptions.set(tickerName, {
        callbacks: [...subscribers.callbacks, callback],
        errorCallbacks: [...subscribers.errorCallbacks, onErrorCallback],
        subFailed: subscribers.subFailed
    });
    subscribeToTickerOnWs(tickerName);
}

export const unsubscribeFromTickerUpdate = tickerName => {
    if (tickersSubscriptions.get(tickerName).subFailed) {
        unsubscribeFromTickerOnWs(tickerName, SECOND_CURRENCY);
    } else {
        unsubscribeFromTickerOnWs(tickerName);
    }
    tickersSubscriptions.delete(tickerName);
}

export const getCurrencyList = async () => {
    let currencyList = null;
    await axios.get(API_CURRENCY_LIST_URL)
        .then(response => {
            currencyList = Object.values(response.data.Data).sort((a, b) => {
                if (a.Symbol > b.Symbol) {
                    return 1;
                }
                if (a.Symbol < b.Symbol) {
                    return -1;
                }
                return 0;
            });
        })
    return currencyList;
}