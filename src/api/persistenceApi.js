const LOCAL_STORAGE_ID = "crypt-list"

export const saveTickersStateToLocalStorage = (tickers) => {
    if (localStorage) {
        localStorage.setItem(LOCAL_STORAGE_ID, JSON.stringify(tickers));
    }
}

export const getTickersStateFromLocalStorage = () => {
    if (localStorage) {
        return localStorage.getItem(LOCAL_STORAGE_ID);
    }
    return null
}