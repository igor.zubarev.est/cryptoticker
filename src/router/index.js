import { createRouter, createWebHistory } from 'vue-router'
import App from '../App.vue'

const routes = [{
  path: '/',
  name: 'App',
  component: App
}]

const router = createRouter({
  mode: 'history',
  history: createWebHistory('/'),
  routes
})

export default router
