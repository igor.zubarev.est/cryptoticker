import {createStore, createLogger} from 'vuex'
import * as persistence from '@/api/persistenceApi'
import {ACTIONS} from "@/store/constants.js";

export const store = createStore({
    state() {
        return {
            tickersList: []
        }
    },

    mutations: {
        [ACTIONS.ADD_TICKER_TO_LIST] (state, ticker) {
            state.tickersList = [...state.tickersList, ticker]
        },

        [ACTIONS.REMOVE_TICKER_FROM_LIST] (state, ticker) {
            state.tickersList = state.tickersList.filter( t => t.id !== ticker.id)
        },

        [ACTIONS.SET_TICKERS_LIST] (state, tickers) {
            state.tickersList = tickers
        }
    },

    actions: {
        [ACTIONS.ADD_TICKER_TO_LIST] ({ commit }, ticker) {
            commit(ACTIONS.ADD_TICKER_TO_LIST, ticker)
        },

        [ACTIONS.REMOVE_TICKER_FROM_LIST] ({ commit }, ticker) {
            commit(ACTIONS.REMOVE_TICKER_FROM_LIST, ticker)
        },

        [ACTIONS.SET_TICKERS_LIST] ({ commit }, tickers) {
            commit(ACTIONS.SET_TICKERS_LIST, tickers)
        },

        [ACTIONS.LOAD_TICKERS_LIST_FROM_STORAGE] ({ commit }) {
            const tickersData = persistence.getTickersStateFromLocalStorage()
            if (tickersData) {
                commit(ACTIONS.SET_TICKERS_LIST, JSON.parse(tickersData))
            }
        },

        [ACTIONS.SAVE_TICKERS_LIST_TO_STORAGE] ({ state }) {
            persistence.saveTickersStateToLocalStorage(state.tickersList)
        }
    },

    plugins: [createLogger()]

})